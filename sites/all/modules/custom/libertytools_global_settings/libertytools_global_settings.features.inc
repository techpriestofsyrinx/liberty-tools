<?php
/**
 * @file
 * libertytools_global_settings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function libertytools_global_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "backup_migrate" && $api == "backup_migrate_exportables") {
    return array("version" => "1");
  }
}
