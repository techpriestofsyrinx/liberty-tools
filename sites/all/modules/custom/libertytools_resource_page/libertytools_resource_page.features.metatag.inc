<?php
/**
 * @file
 * libertytools_resource_page.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function libertytools_resource_page_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:resource.
  $config['node:resource'] = array(
    'instance' => 'node:resource',
    'config' => array(
      'description' => array(
        'value' => '[node:field-resource-description:summary]',
      ),
      'abstract' => array(
        'value' => '[node:field-category]',
      ),
      'keywords' => array(
        'value' => '[node:field-resource-tags]',
      ),
      'og:see_also' => array(
        'value' => '[node:field-links]',
      ),
    ),
  );

  return $config;
}
