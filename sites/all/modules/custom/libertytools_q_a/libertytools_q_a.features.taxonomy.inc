<?php
/**
 * @file
 * libertytools_q_a.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function libertytools_q_a_taxonomy_default_vocabularies() {
  return array(
    'question_category' => array(
      'name' => 'Question Category',
      'machine_name' => 'question_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
