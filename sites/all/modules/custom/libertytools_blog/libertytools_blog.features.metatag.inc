<?php
/**
 * @file
 * libertytools_blog.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function libertytools_blog_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:blog_post.
  $config['node:blog_post'] = array(
    'instance' => 'node:blog_post',
    'config' => array(
      'abstract' => array(
        'value' => '[node:summary]',
      ),
      'keywords' => array(
        'value' => '[node:field-categories]',
      ),
      'og:image' => array(
        'value' => '[node:field-project-image]',
      ),
    ),
  );

  return $config;
}
